package com.example.mykurs;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class SRenderer implements GLSurfaceView.Renderer{

    private final MainActivity mShadowsActivity;
    private MyRender mSimpleShadowProgram;
    private MyRender mDepthMapProgram;
    private int mActiveProgram;

    private final float[] mMVPMatrix = new float[16];
    private final float[] mMVMatrix = new float[16];
    private final float[] mNormalMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    private final float[] mModelMatrix = new float[16];

    private final float[] mLightPosInEyeSpace = new float[16];
    private final float[] mLightPosModel = new float[]
            {0.0f, 0.0f, 0.0f, 0.0f};
    private float[] mActualLightPosition = new float[4];

    private int mDisplayWidth,mDisplayHeight;
    private int[] fboId;
    private int[] renderTextureId;

    private int sc_mvpMatrixUniform;
    private int sc_mvMatrixUniform;
    private int sc_normalMatrixUniform;
    private int sc_lightPosUniform;
    private int sc_shadowProjMatrixUniform;
    private int shadow_mvpMatrixUniform;

    private int sc_positionAttr, sc_normalAttr,sc_colorAttr;
    private Context c;
    private Obj Table, Apple, Cup, Bananas,Potato,Pear;

    SRenderer(final MainActivity shadowsActivity, Context c) {
        mShadowsActivity = shadowsActivity;
        this.c = c;
    }
    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        //вкл тест глубины
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        //берутся объекты из файла с определенным цветом
        Table = new Obj(c, new float[]{0.7f, 0.0f, 0.4f, 1.0f}, "Table.obj");
        Cup = new Obj(c, new float[]{0.4f, 0.2f, 0.0f, 1.0f}, "cup.obj");
        Apple = new Obj(c, new float[]{0.0f, 0.7f, 0.0f, 1.0f}, "apple.obj");
        Bananas = new Obj(c, new float[]{0.8f, 0.8f, 0.0f, 1.0f}, "Banana.obj");
        Potato = new Obj(c, new float[]{0.9f, 0.6f, 0.3f, 1.0f}, "potatoo.obj");
        Pear = new Obj(c, new float[]{0.0f, 0.7f, 0.0f, 1.0f}, "pear13.obj");
        //шейдер тени
        mSimpleShadowProgram = new MyRender(R.raw.depth_tex_v_with_shadow, R.raw.depth_tex_f_with_simple_shadow, mShadowsActivity);
        //шейдер глубины
        mDepthMapProgram = new MyRender(R.raw.depth_tex_v_depth_map, R.raw.depth_tex_f_depth_map, mShadowsActivity);
        mActiveProgram = mSimpleShadowProgram.getProgram();
    }

    private void genShadowFBO() {
        fboId = new int[1];
        renderTextureId = new int[1];
        //генерирует имена обектов фреймбуфера
        GLES20.glGenFramebuffers(1, fboId, 0);
        //генерирует имя текстуры
        GLES20.glGenTextures(1, renderTextureId, 0);
        //привязывает текстуру
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderTextureId[0]);
        //устанавливает параметры текстуры
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        //привязка буфера кадра к точке привязки
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, fboId[0]);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        //размеры дисплея
        mDisplayWidth = width;
        mDisplayHeight = height;
        //устанавливает область просмотра
        GLES20.glViewport(0, 0, mDisplayWidth, mDisplayHeight);

        genShadowFBO();

        float r = (float) mDisplayWidth / mDisplayHeight;
        float bottom = -1.0f,top = 1.5f,near = 0.7f,far = 80.0f;
        //Угол обзора
        Matrix.frustumM(mProjectionMatrix, 0, -r, r, bottom, top, near, far);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        mActiveProgram = mSimpleShadowProgram.getProgram();
        //позиции просмотра объектов
        Matrix.setLookAtM(mViewMatrix, 0,
                4, 4, 0,
                0, 0, 0,
                -1,0,0);
        //возвращает расположение универсальной переменной
        sc_mvpMatrixUniform = GLES20.glGetUniformLocation(mActiveProgram, "uMVPMatrix");
        sc_normalMatrixUniform = GLES20.glGetUniformLocation(mActiveProgram, "uNormalMatrix");
        sc_shadowProjMatrixUniform = GLES20.glGetUniformLocation(mActiveProgram, "uShadowProjMatrix");
        sc_normalAttr = GLES20.glGetAttribLocation(mActiveProgram, "aNormal");
        sc_colorAttr = GLES20.glGetAttribLocation(mActiveProgram, "aColor");

        int shadowMapProgram = mDepthMapProgram.getProgram();
        shadow_mvpMatrixUniform = GLES20.glGetUniformLocation(shadowMapProgram, "uMVPMatrix");

        float[] basicMatrix = new float[16];
        //устанавливает матрицу в единичную
        Matrix.setIdentityM(basicMatrix, 0);
        //сохраняет результат в векторе-столбце
        Matrix.multiplyMV(mActualLightPosition, 0, basicMatrix, 0, mLightPosModel, 0);

        Matrix.setIdentityM(mModelMatrix, 0);
        renderScene();
    }

    private void renderScene() {
        //привязка буфера кадра к точке привязки
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        GLES20.glUseProgram(mActiveProgram);

        GLES20.glViewport(0, 0, mDisplayWidth, mDisplayHeight);

        float[] tempResultMatrix = new float[16];
        //работа с матрицами
        Matrix.multiplyMM(tempResultMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
        System.arraycopy(tempResultMatrix, 0, mMVMatrix, 0, 16);

        GLES20.glUniformMatrix4fv(sc_mvMatrixUniform, 1, false, mMVMatrix, 0);

        Matrix.invertM(tempResultMatrix, 0, mMVMatrix, 0);
        Matrix.transposeM(mNormalMatrix, 0, tempResultMatrix, 0);

        GLES20.glUniformMatrix4fv(sc_normalMatrixUniform, 1, false, mNormalMatrix, 0);

        Matrix.multiplyMM(tempResultMatrix, 0, mProjectionMatrix, 0, mMVMatrix, 0);
        System.arraycopy(tempResultMatrix, 0, mMVPMatrix, 0, 16);

        GLES20.glUniformMatrix4fv(sc_mvpMatrixUniform, 1, false, mMVPMatrix, 0);
        Matrix.multiplyMV(mLightPosInEyeSpace, 0, mViewMatrix, 0, mActualLightPosition, 0);
        GLES20.glUniform3f(sc_lightPosUniform, mLightPosInEyeSpace[0], mLightPosInEyeSpace[1], mLightPosInEyeSpace[2]);

        //отрисовка предметов
        Table.render(sc_positionAttr,sc_normalAttr, sc_colorAttr,false);
        Cup.render(sc_positionAttr,sc_normalAttr, sc_colorAttr, false);
        Apple.render(sc_positionAttr,sc_normalAttr, sc_colorAttr, false);
        Bananas.render(sc_positionAttr,sc_normalAttr, sc_colorAttr,  false);
        Potato.render(sc_positionAttr,sc_normalAttr, sc_colorAttr, false);
        Pear.render(sc_positionAttr,sc_normalAttr, sc_colorAttr,  false);
    }
}
