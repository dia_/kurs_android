package com.example.mykurs;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GLSurfaceView mGLSurfaceView = new GLSurfaceView(this);

        mGLSurfaceView.setEGLContextClientVersion(2);

        SRenderer renderer = new SRenderer(this, this);
        mGLSurfaceView.setRenderer(renderer);

        setContentView(mGLSurfaceView);
    }
}